package com.pnut.activities;

import android.app.Activity;
import android.os.Bundle;
import com.pnut.R;

/**
 * Created by Claudio Carcaci on 13 Feb 2017.
 */
public class Foo extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_foo);
	}
}
