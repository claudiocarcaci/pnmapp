package com.pnut.components;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Claudio Carcaci on 20 Mar 2017.
 */
public class TextViewStyled extends TextView {
	public TextViewStyled(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public TextViewStyled(Context context) {
		super(context);
		init();
	}

	public TextViewStyled(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TextViewStyled(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
	}

	private void init() {
		if(!isInEditMode()) {
			Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
			setTypeface(typeface);
		}
	}
}
